import {React, useState } from 'react';
import Swal from "sweetalert2";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

const AddTask = ({ onSave }) => {
    const [titre, setTitre] = useState('');
    const [date, setDate] = useState(new Date());
    const [description, setDescription] = useState('');
    const [doneStatus, setDoneStatus] = useState("attente");

    const onSubmit = (e) => {
        e.preventDefault();

        if (!titre && !date && !description) {
            Swal.fire({
                icon: 'erreur',
                title: 'Oops...',
                text: 'remplir tous les champs!'
            })
        } else if (!titre && date) {
            Swal.fire({
                icon: 'erreur',
                title: 'Oops...',
                text: 'Fill in your task!'
            })
        } else if (titre && !date) {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'enter date!'
            })
        } else {
            onSave({ titre, date, description, doneStatus });
        }

        setTitre('');
        setDate(new Date());
        setDescription("");
        setDoneStatus('');
    }

    return ( <form className = "add-form" onSubmit = { onSubmit } >
        <div className = "form-control" >
        <label > Tache </label> 
        <input type = "text"
        placeholder = "nouvelle tache"
        value = { titre }
        onChange = {(e) => setTitre(e.target.value) }/> 
        </div> 
        <div className = "form-control" >
        <label> Date </label> 
        <DatePicker selected={date}
        onChange = {(date) => setDate(date)}  showTimeSelect   dateFormat="MMMM eeee d, yyyy h:mm aa"/> 
        </div>

        <div className = "form-control" >
        <label> Description </label> 
        <input type = "text"
        placeholder = "description"
        value = { description }
        onChange = {(e) => setDescription(e.target.value) }/> 
        </div>

{/*         <div class = "form-control" >
        <label> Status </label> 
        <select name="tache" value = { doneStatus }
        onChange = {(e) => setDoneStatus(e.target.value) }/> 
        </div>
 */}
        <input type = "submit"
        className = "btn btn-block"
        value = "créer" />
        </form>
    )
}

export default AddTask
