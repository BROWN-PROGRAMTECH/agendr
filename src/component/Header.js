import React from 'react';
import "../index.css";
import Button from './Button'
import { Typography, CssBaseline, Card, CardContent, CardActions, CardMedia, Grid, Toolbar, Container, AppBar } from '@material-ui/core';
import AccessTimeFilledOutlinedIcon from '@mui/icons-material/AccessTimeFilledOutlined';
import Icon from '@mui/material/Icon';

const Header = ({ showForm, changeTextAndColor }) => {
    return (
       <>
        <CssBaseline/>
        <AppBar>
            <Toolbar position="relative">
                <AccessTimeFilledOutlinedIcon/>
                <Typography variant="h3" align='center'>Agendr</Typography>
            </Toolbar>
            <Button  onClick={showForm} color={changeTextAndColor ? 'white' : 'white'} text={changeTextAndColor ? 'X Fermer' : '+ Ajouter'} />
        </AppBar>
        {/* <div position='right'>                    
                    <Button  onClick={showForm} color={changeTextAndColor ? 'red' : 'green'} text={changeTextAndColor ? 'Fermer' : 'Ajouter'} />
        </div> */}
       </>
    )
}

export default Header;
